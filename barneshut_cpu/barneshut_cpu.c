/*
 * Barnes-Hut N-body simulation, running on CPU
 * (c) Teemu Peltonen & Ville Vaskonen
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/stat.h>
#include <math.h>
#include <sys/time.h>

#define sign(x) (( x > 0 ) - ( x < 0 ))

#define n 1024*10 // number of particles, multiple of local_work_size
#define N 5*n // size of long arrays
#define M 100 // max number of timesteps

#define A 0.5f // softening constant (plummer model)
#define SOFTENING 0.1f // another softening constant (gravity)
#define G 50.0 // gravitational constant

#define MAXDEPTH 32
#define THETA 0.25f
#define BBLIMIT 0.0001f
#define DAMPING 0.00001f
#define TIMESTEP 0.0001f

const char *data = "data.dat";

void boundingBox(float *x, float *y, float *size) {
	
	float minx, miny, maxx, maxy;
	minx = maxx = x[0];
	miny = maxy = y[0];
	// calculate global minimum and maximum
	int j;
	for(j=1; j<n; j++) {
		minx = fmin(minx,x[j]);
		miny = fmin(miny,y[j]);
		maxx = fmax(maxx,x[j]);
		maxy = fmax(maxy,y[j]);
	}
	size[N-n-1] = fmax(maxx-minx, maxy-miny)/2.0f;
	x[N-1] = maxx - size[N-n-1];
	y[N-1] = maxy - size[N-n-1];
}

void buildTree(float *x, float *y, float *size, int *lu,
				int *ru, int *ld, int *rd,
				int *cells_no, int *mass, float *vx, float *vy) {

	int j; // long index of the current node
	int k; // short index of the current node
	int prev, prevk; // previous j and k
	int a;
	
    int gid = 0;
	while(gid<n) {
		
		float px = x[gid];
		float py = y[gid];
		
		int flag = 0;
		
		j = N-1;
		prev = N-1;
		k = j-n;
		
		// first thread inserts first body
		if (gid==0) {
			// find the quarter where the body belongs
			if(px<x[j]) {
				if(py<y[j])
					lu[k] = gid;
				else
					ld[k] = gid;
			} else {
				if(py<y[j])
					ru[k] = gid;
				else
					rd[k] = gid;
			}
			flag = 1;
		}
		
		if(mass[gid]!=0) {
			while(flag!=1) {
				// find the quarter where the body belongs
				while(j>=n) {
					prev = j;
					prevk = k;
					if(px<x[j]) {
						if(py<y[j])
							j = lu[k];
						else
							j = ld[k];
					} else {
						if(py<y[j])
							j = ru[k];
						else
							j = rd[k];
					}
					k = j-n;
				}
				a = j = prev;
				k = prevk;
				
				// find the quarter where the body belongs
				if(px<x[j]) {
					if(py<y[j])
						j = lu[k];
					else
						j = ld[k];
				} else {
					if(py<y[j])
						j = ru[k];
					else
						j = rd[k];
				}
				k = j-n;
				
				// if jth node is empty
				if(j<0) {
					// insert body
					if(px<x[prev]) {
						if(py<y[prev])
							lu[prevk] = gid;
						else
							ld[prevk] = gid;
					} else {
						if(py<y[prev])
							ru[prevk] = gid;
						else
							rd[prevk] = gid;
					}
					flag = 1;
				}
				
				// if j is a leaf node
				if(k<0 && j>=0) {
					prevk = prev-n;
					float psize  = size[prevk]/2.0f;
					// if bodies are very close each other
					if(psize<BBLIMIT) {
						// make one big body
						int m1 = mass[j];
						int m2 = mass[gid];
						vx[j] = (vx[j]*m1 + vx[gid]*m2)/(m1+m2);
						vy[j] = (vy[j]*m1 + vy[gid]*m2)/(m1+m2);
						vx[gid] = 0.0f;
						vy[gid] = 0.0f;
						mass[j] += m2;
						mass[gid] = 0;
						flag = 1;
					} else {
						int ncj, nck;
						int cn = cells_no[0]++;
						ncj = N-cn-1; // new cell index in the long arrays
						nck = ncj-n; // new cell index in the short arrays
						
						// create a new internal node in the right place
						size[nck] = psize;
						if(lu[prevk]==j) {
							lu[prevk] = ncj;
							x[ncj] = x[prev]-psize;
							y[ncj] = y[prev]-psize;
						} else if(ru[prevk]==j) {
							ru[prevk] = ncj;
							x[ncj] = x[prev]+psize;
							y[ncj] = y[prev]-psize;
						} else if(ld[prevk]==j) {
							ld[prevk] = ncj;
							x[ncj] = x[prev]-psize;
							y[ncj] = y[prev]+psize;
						} else {
							rd[prevk] = ncj;
							x[ncj] = x[prev]+psize;
							y[ncj] = y[prev]+psize;
						}
						// insert previous body
						if(x[j]<x[ncj]) {
							if(y[j]<y[ncj])
								lu[nck] = j;
							else
								ld[nck] = j;
						} else {
							if(y[j]<y[ncj])
								ru[nck] = j;
							else
								rd[nck] = j;
						}
						
						j = ncj;
						k = nck;
					}
				}
			}
		} 
		gid++; 
	}
}

void summarizeTree(float *x, float *y, int *lu, int *ru, int *ld, int *rd, int *mass, int *cells_no) {
	int gid = 0;
	
	// index of the last body
	int k = N-cells_no[0]-n;
	int j = k + gid;
	
	int pmass = 0; 	// cumulative mass
	// center of mass
	float cx = 0;
	float cy = 0;
	
	int counter = 0;
	int idx;
	
	// iterate over all internal nodes
	while(j<N-n) {
		counter = 0;
		// until all childs are ready
		while(counter<4) {
			cx = 0;
			cy = 0;
			counter = 0;
			pmass = 0;
			
			idx = lu[j];
			if(idx!=-1) {
				if(mass[idx]!=-1) {
					pmass += mass[idx];
					cx += mass[idx]*x[idx];
					cy += mass[idx]*y[idx];
					counter++;
				}
			} else counter++;
			
			idx = ru[j];
			if(idx!=-1) {
				if(mass[idx]!=-1) {
					pmass += mass[idx];
					cx += mass[idx]*x[idx];
					cy += mass[idx]*y[idx];
					counter++;
				}
			} else counter++;
			
			idx = ld[j];
			if(ld[j]!=-1) {
				if(mass[idx]!=-1) {
					pmass += mass[idx];
					cx += mass[idx]*x[idx];
					cy += mass[idx]*y[idx];
					counter++;
				}
			} else counter++;
			
			idx = rd[j];
			if(idx!=-1) {
				if(mass[idx]!=-1) {
					pmass += mass[idx];
					cx += mass[idx]*x[idx];
					cy += mass[idx]*y[idx];
					counter++;
				}
			} else counter++;
			
			if(counter==4) {
				// insert cumulative mass and center of mass to the internal node
				x[j+n] = cx/pmass;
				y[j+n] = cy/pmass;
				mass[j+n] = pmass;
			}
			
		}
		j ++;
	} 
}

void sort(int *lu, int *ru, int *ld, int *rd, int *mass, int *sorted) {
	
	int gid = 0;
	while(gid<n) {
		
		int idx = N-1;
		int pmass = 0;
		// iterate over all internal nodes
		while(idx>n) {
			int cid = lu[idx-n];
			// check if node has childs
			if(cid!=-1) {
				pmass += mass[cid];
				// divide workitems according to the structure of the tree
				if(gid<pmass) {
					pmass -= mass[cid];
					idx = cid;
					continue;
				}
			}
			
			cid = ru[idx-n];
			if(cid!=-1) {
				pmass += mass[cid];
				if(gid<pmass) {
					pmass -= mass[cid];
					idx = cid;
					continue;
				}
			}
			
			cid = ld[idx-n];
			if(cid!=-1) {
				pmass += mass[cid];
				if(gid<pmass) {
					pmass -= mass[cid];
					idx = cid;
					continue;
				}
			}
			
			cid = rd[idx-n];
			if(cid!=-1) {
				pmass += mass[cid];
				if(gid<pmass) {
					pmass -= mass[cid];
					idx = cid;
					continue;
				}
			}
		} 
		sorted[pmass] = idx;
		gid++; 
	}
}

void calculateForces(float *x, float *y, float *ax, float *ay,
						int *lu, int *ru, int *ld, int *rd,
						int *mass, int *sorted, float *size) {

    int gid = 0; 
	while(gid<n) {
		
		// get index of the body
		int idx = sorted[gid];		
		if(idx!=-1) {
			// iteration stack
			int stack[4*MAXDEPTH];
			float lx, ly;
			float lmass, ls;
			
			int l;
			float d;
			
			int k = 0;
			// initialize stack
			stack[k] = N-1;
			
		
			float px = 0, py = 0, pax = 0, pay = 0;
			px = x[idx], py = y[idx];
			
			while(k>=0) {
				// check if node is empty
				if(stack[k]==-1) {
					k--;
					continue;
				}
				
				lx = x[stack[k]];
				ly = y[stack[k]];
				lmass = mass[stack[k]];
				if(stack[k]>=n)
					ls = size[stack[k]-n];
				
				// distance squared
				d = (px-lx)*(px-lx) + (py-ly)*(py-ly);
				
				// check also if current node is a body
				if(stack[k]<n || d*THETA>ls) {
					// check also that if node is a body it is not the body itself
					if(stack[k]!=idx) {
						d = 1.0f/sqrt(d+SOFTENING);
						d = G*lmass*d*d*d;
						pax += d*(lx-px);
						pay += d*(ly-py);
					}
					k--;
				} else {
					// insert childs to iteration stack
					l = stack[k]-n;
					stack[k++] = ld[l];
					stack[k++] = rd[l];
					stack[k++] = ru[l];
					stack[k] = lu[l];
				}
			}
			ax[idx] = pax;
			ay[idx] = pay;
		} 
		gid++;
	}
}

void updatePositions(float *x, float *y, float *vx, float *vy, float *ax, float *ay, int *mass) {
	int gid = 0;
	while(gid<n) {
		int m = mass[gid];
		float pvx = vx[gid];
		float pvy = vy[gid];
		
		if(m!=0) {
			// velocity damping
			float v2 = pvx*pvx + pvy*pvy;
			float pax = ax[gid] -= sign(pvx)*DAMPING*v2/m;
			float pay = ay[gid] -= sign(pvy)*DAMPING*v2/m;
			
			// update position
			x[gid] += TIMESTEP*pvx + 0.5*pax*TIMESTEP*TIMESTEP;
			y[gid] += TIMESTEP*pvy + 0.5*pay*TIMESTEP*TIMESTEP;
			
			// calculate new velocity
			pvx += TIMESTEP*ax[gid];
			pvy += TIMESTEP*ay[gid];
			
			vx[gid] = pvx;
			vy[gid] = pvy;
		}
		gid++; 
	}
}


// print arrays to file
void fprintArrays(float *a, float *b, int *c, int m) {
	FILE *fp;
	fp = fopen(data, "a");
	int i;
	for (i=0; i<m; i++) {
		fprintf(fp, "%f    %f    %d\n", a[i],b[i], c[i]);
	}
	fclose(fp);
}

// print number of particles to file
void fprintScalar(int a) {
	FILE *fp;
	fp = fopen(data, "a");
	fprintf(fp, "%d\n", a);
	fclose(fp);
}

// generates random numbers according to plummer model
float plummerRand(float rmax, float mmax) {
	float R = rmax*rand()/(float)RAND_MAX;
	float P = mmax*rand()/(float)RAND_MAX;
	float m = mmax*R*R*R/pow(R*R+A*A, 1.5f);
	while (m>P) {
		R = rmax*rand()/(float)RAND_MAX;
		P = mmax*rand()/(float)RAND_MAX;
		m = mmax*R*R*R/pow(R*R+A*A, 1.5f);
	}
	return R;
}

// orbital speed according to plummer model mass distribution
double orbitalSpeed(float R, float mmax) {
	return sqrt(G*mmax*R*R/pow(R*R+A*A, 1.5f));
}

void initialize(float *x, float *y, float *ax, float *ay, int *lu, int *ru, int *ld, int *rd,
				int *mass, float *size, int *semaphor, int *cells_no, int *sorted) {
	int i;
	for (i=n; i<N; i++) {
		mass[i] = -1;
	}
	for (i=0; i<N-n; i++) {
		lu[i] = ru[i] = ld[i] = rd[i] = -1;
		size[i] = 0;
	}
	cells_no[0] = 1;
	for (i=0; i<N; i++) {
		semaphor[i] = -1;
	}
	for (i=n; i<N; i++) {
		x[i] = 0;
		y[i] = 0;
	}
	for (i=0; i<n; i++) {
		ax[i] = 0;
		ay[i] = 0;
	}
	for (i=0; i<n; i++) {
		sorted[i] = -1;
	}
}

int main (int argc, char *argv[]) {
	// declare time variables: 
	struct timeval start;
	struct timeval end;
	struct timeval kt1;
	struct timeval kt2;
	long t1, t2;
	gettimeofday(&start, NULL);
	
	srand(5.0f);
	float times[M][6];

	// allocate memory for n particles
	float *x = (float *)malloc(N*sizeof(float));
	float *y = (float *)malloc(N*sizeof(float));
	float *vx = (float *)malloc(n*sizeof(float));
	float *vy = (float *)malloc(n*sizeof(float));
	int *mass = (int *)malloc(N*sizeof(int));
	float *ax = (float *)malloc(n*sizeof(float));
	float *ay = (float *)malloc(n*sizeof(float));
	float *size = (float *)malloc((N-n)*sizeof(float));
	int *lu = (int *)malloc((N-n)*sizeof(int));
	int *ru = (int *)malloc((N-n)*sizeof(int));
	int *ld = (int *)malloc((N-n)*sizeof(int));
	int *rd = (int *)malloc((N-n)*sizeof(int));
	int *semaphor = (int *)malloc(N*sizeof(int));
	int *cells_no = (int *)malloc(sizeof(int));
	int *sorted = (int *)malloc(n*sizeof(int));

	float r, theta, v;

	// initialize positions, velocities and masses of bodies
    initialize(x, y, ax, ay, lu, ru, ld, rd, mass, size, semaphor, cells_no, sorted);

	int i;
	for (i=0; i<n/4; i++) {
        r = plummerRand(0.5f,n/4.0f);
        theta = rand()/(float)RAND_MAX*8*atan(1.0f);

        x[i] = r*cos(theta)-0.4f;
        y[i] = r*sin(theta)-0.4f;

        v = orbitalSpeed(r, n/4.0f);
        vx[i] = v*cos(theta-atan(1.0f)*2.0f);
        vy[i] = v*sin(theta-atan(1.0f)*2.0f);

        vx[i] += 250.0f;
        vy[i] += 0.0f;
	}

	 for (i=n/4; i<n; i++) {
         r = plummerRand(1.2f,3*n/4.0f);
         theta = rand()/(float)RAND_MAX*8*atan(1.0f);

         x[i] = r*cos(theta)+1.4f;
         y[i] = r*sin(theta)+1.4f;

         v = orbitalSpeed(r, 3*n/4.0f);
         vx[i] = v*cos(theta-atan(1.0f)*2.0f);
         vy[i] = v*sin(theta-atan(1.0f)*2.0f);

         vx[i] += -200.0f;
         vy[i] += 0.0f;
    }

	for (i=0; i<n; i++) {
		mass[i] = 1;
	}

	// remove old data.dat file
	remove(data);
	// print number of particles to new data.dat file
	fprintScalar(n);
	
	for (i=0; i<M; i++) {
		printf("%d\n", i);

		initialize(x, y, ax, ay, lu, ru, ld, rd, mass, size, semaphor, cells_no, sorted);
		// write  position and mass to file data.dat
		fprintArrays(x,y,mass,n);
		
		gettimeofday(&kt1, NULL);
		boundingBox(x, y, size);
		gettimeofday(&kt2, NULL);
		t1 = kt1.tv_sec*1000000 + (kt1.tv_usec);
		t2 = kt2.tv_sec*1000000  + (kt2.tv_usec);
		printf("0: %f\n" ,(t2-t1)/1e6f);
		times[i][0] = (t2-t1)/1e6f;
		
		gettimeofday(&kt1, NULL);
		buildTree(x, y, size, lu, ru, ld, rd, cells_no, mass, vx, vy);
		gettimeofday(&kt2, NULL);
		t1 = kt1.tv_sec*1000000 + (kt1.tv_usec);
		t2 = kt2.tv_sec*1000000  + (kt2.tv_usec);
		printf("1: %f\n" ,(t2-t1)/1e6f);
		times[i][1] = (t2-t1)/1e6f;
		
		gettimeofday(&kt1, NULL);
		summarizeTree(x, y, lu, ru, ld, rd, mass, cells_no);
		gettimeofday(&kt2, NULL);
		t1 = kt1.tv_sec*1000000 + (kt1.tv_usec);
		t2 = kt2.tv_sec*1000000  + (kt2.tv_usec);
		printf("2: %f\n" ,(t2-t1)/1e6f);
		times[i][2] = (t2-t1)/1e6f;
		
		gettimeofday(&kt1, NULL);
		sort(lu, ru, ld, rd, mass, sorted);
		gettimeofday(&kt2, NULL);
		t1 = kt1.tv_sec*1000000 + (kt1.tv_usec);
		t2 = kt2.tv_sec*1000000  + (kt2.tv_usec);
		printf("3: %f\n" ,(t2-t1)/1e6f);
		times[i][3] = (t2-t1)/1e6f;
		
		gettimeofday(&kt1, NULL);
		calculateForces(x, y, ax, ay, lu, ru, ld, rd, mass, sorted, size);
		gettimeofday(&kt2, NULL);
		t1 = kt1.tv_sec*1000000 + (kt1.tv_usec);
		t2 = kt2.tv_sec*1000000  + (kt2.tv_usec);
		printf("4: %f\n" ,(t2-t1)/1e6f);
		times[i][4] = (t2-t1)/1e6f;
		
		gettimeofday(&kt1, NULL);
		updatePositions(x, y, vx, vy, ax, ay, mass);
		gettimeofday(&kt2, NULL);
		t1 = kt1.tv_sec*1000000 + (kt1.tv_usec);
		t2 = kt2.tv_sec*1000000  + (kt2.tv_usec);
		printf("5: %f\n" ,(t2-t1)/1e6f);
		times[i][5] = (t2-t1)/1e6f;		
	}

	int j;
	for (j=0; j<6; j++) {
		float t = 0;
		int k;
		for (k=0; k<M; k++) {
			t += times[k][j];
		}
		t /= M;
		printf("kernel %d average time: %f\n", j, t);
	}
	
	// free up memory
	free(x), free(y), free(vx), free(vy), free(mass);
	free(ax), free(ay);
	free(size), free(lu), free(ru), free(ld), free(rd);
	free(semaphor), free(cells_no), free(sorted);
	
	gettimeofday(&end, NULL);
	t1 = start.tv_sec*1000000 + (start.tv_usec);
	t2 = end.tv_sec*1000000  + (end.tv_usec);
    printf("total time: %f\n" ,(t2-t1)/1e6f);
    return 0;
}
