/*
 * Brute-force N-body simulation, running on GPU
 * (c) Teemu Peltonen & Ville Vaskonen
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/stat.h>
#include <math.h>
#include <sys/time.h>

#ifdef APPLE
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define n 1024*10 // number of particles, multiple of local_work_size
#define M 100 // max number of timesteps

#define A 0.5f // softening constant (plummer model)
#define SOFTENING 0.1f // another softening constant (gravity)
#define G 50.0 // gravitational constant

const char *data = "data.dat";
const char *filename = "source.cl";

const char* oclErrorString(cl_int error) {
    static char errorString[][64] = {
        "CL_SUCCESS",
        "CL_DEVICE_NOT_FOUND",
        "CL_DEVICE_NOT_AVAILABLE",
        "CL_COMPILER_NOT_AVAILABLE",
        "CL_MEM_OBJECT_ALLOCATION_FAILURE",
        "CL_OUT_OF_RESOURCES",
        "CL_OUT_OF_HOST_MEMORY",
        "CL_PROFILING_INFO_NOT_AVAILABLE",
        "CL_MEM_COPY_OVERLAP",
        "CL_IMAGE_FORMAT_MISMATCH",
        "CL_IMAGE_FORMAT_NOT_SUPPORTED",
        "CL_BUILD_PROGRAM_FAILURE",
        "CL_MAP_FAILURE",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "CL_INVALID_VALUE",
        "CL_INVALID_DEVICE_TYPE",
        "CL_INVALID_PLATFORM",
        "CL_INVALID_DEVICE",
        "CL_INVALID_CONTEXT",
        "CL_INVALID_QUEUE_PROPERTIES",
        "CL_INVALID_COMMAND_QUEUE",
        "CL_INVALID_HOST_PTR",
        "CL_INVALID_MEM_OBJECT",
        "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",
        "CL_INVALID_IMAGE_SIZE",
        "CL_INVALID_SAMPLER",
        "CL_INVALID_BINARY",
        "CL_INVALID_BUILD_OPTIONS",
        "CL_INVALID_PROGRAM",
        "CL_INVALID_PROGRAM_EXECUTABLE",
        "CL_INVALID_KERNEL_NAME",
        "CL_INVALID_KERNEL_DEFINITION",
        "CL_INVALID_KERNEL",
        "CL_INVALID_ARG_INDEX",
        "CL_INVALID_ARG_VALUE",
        "CL_INVALID_ARG_SIZE",
        "CL_INVALID_KERNEL_ARGS",
        "CL_INVALID_WORK_DIMENSION",
        "CL_INVALID_WORK_GROUP_SIZE",
        "CL_INVALID_WORK_ITEM_SIZE",
        "CL_INVALID_GLOBAL_OFFSET",
        "CL_INVALID_EVENT_WAIT_LIST",
        "CL_INVALID_EVENT",
        "CL_INVALID_OPERATION",
        "CL_INVALID_GL_OBJECT",
        "CL_INVALID_BUFFER_SIZE",
        "CL_INVALID_MIP_LEVEL",
        "CL_INVALID_GLOBAL_WORK_SIZE",
    };
    return errorString[-error];
}

void pfn_notify(const char *errinfo, const void *private_info, size_t cb, void *user_data) {
	fprintf(stderr, "%s\n", errinfo);
}

// read source from file
char *loadProgramSource(const char *filename) { 
	struct stat statbuf;
	FILE *fh; 
	char *source; 
	
	fh = fopen(filename, "r");
	if (fh == 0)
		return 0; 
	
	stat(filename, &statbuf);
	source = (char *) malloc(statbuf.st_size + 1);
	fread(source, statbuf.st_size, 1, fh);
	source[statbuf.st_size] = '\0';
	
	return source; 
}

// print arrays to file
void fprintArrays(float *a, float *b, int *c, int m) {
	FILE *fp;
	fp = fopen(data, "a");
	int i;
	for (i=0; i<m; i++) {
		fprintf(fp, "%f    %f    %d\n", a[i],b[i], c[i]);
	}
	fclose(fp);
}

// print number of particles to file
void fprintScalar(int a) {
	FILE *fp;
	fp = fopen(data, "a");
	fprintf(fp, "%d\n", a);
	fclose(fp);
}

// generates random numbers according to plummer model
float plummerRand(float rmax, float mmax) {
	float R = rmax*rand()/(float)RAND_MAX;
	float P = mmax*rand()/(float)RAND_MAX;
	float m = mmax*R*R*R/pow(R*R+A*A, 1.5f);
	while (m>P) {
		R = rmax*rand()/(float)RAND_MAX;
		P = mmax*rand()/(float)RAND_MAX;
		m = mmax*R*R*R/pow(R*R+A*A, 1.5f);
	}
	return R;
}

// orbital speed according to plummer model mass distribution
double orbitalSpeed(float R, float mmax) {
	return sqrt(G*mmax*R*R/pow(R*R+A*A, 1.5f));
}

void initialize(float *ax, float *ay) {
	int i;
	for (i=0; i<n; i++) {
		ax[i] = 0;
		ay[i] = 0;
	}
}

int runCL(float *x, float *y, float *vx, float *vy, int *mass) {
	float times[M][2];
	float gputime = 0;
	
	cl_program program[1];
	cl_kernel kernel[2];
	cl_command_queue cmd_queue;
	cl_context context;
	cl_platform_id platform;
	cl_uint num_platforms;
	cl_device_id device = NULL;
	cl_int err = 0;

	err = clGetPlatformIDs(1, &platform, &num_platforms);
	
	// find the GPU CL device
	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
	//printf("clGetDeviceIDs %s\n", oclErrorString(err));
	assert(device);

	// get some information about the device
	size_t returned_size = 0;
	cl_char vendor_name[1024] = {0};
	cl_char device_name[1024] = {0};
	cl_char driver_version[1024] = {0};
	cl_char device_version[1024] = {0};
	cl_char extensions[1024] = {0};
	size_t max_work_size = 0;
	cl_uint max_compute_units = 0;
	cl_uint max_clock_freq = 0;
	cl_ulong local_mem_size = 0;
	cl_ulong global_mem_size = 0;
	
	err = clGetDeviceInfo(device, CL_DEVICE_VENDOR, sizeof(vendor_name), vendor_name, &returned_size);
	err |= clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(device_name), device_name, &returned_size);
	err |= clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), &max_compute_units, &returned_size);
	err |= clGetDeviceInfo(device, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(cl_uint), &max_clock_freq, &returned_size);
	err |= clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &max_work_size, &returned_size);
	err |= clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(cl_ulong), &local_mem_size, &returned_size);
	err |= clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), &global_mem_size, &returned_size);
	err |= clGetDeviceInfo(device, CL_DEVICE_VERSION, sizeof(device_version), device_version, &returned_size);
	err |= clGetDeviceInfo(device, CL_DRIVER_VERSION, sizeof(driver_version), driver_version, &returned_size);
	err |= clGetDeviceInfo(device, CL_DEVICE_EXTENSIONS, sizeof(extensions), extensions, &returned_size);
	//printf("clGetDeviceInfo %s\n", oclErrorString(err));
	assert(err == CL_SUCCESS);
	
	printf("\n");
	printf("Connecting to %s %s...\n", vendor_name, device_name);
	printf("Number of compute units: %d\n", (int)max_compute_units);
	printf("Clock frequency: %d MHz\n", (int)max_clock_freq);
	printf("Work group size: %d\n", (int)max_work_size);
	printf("Local memory size: %d\n", (int)local_mem_size);
	printf("Global memory size: %d\n", (int)global_mem_size);
	printf("Driver version: %s\n", driver_version);
	printf("Device version: %s\n", device_version);
	printf("Extensions: %s\n", extensions);
	printf("\n");
	
	size_t local_work_size = max_work_size;
	size_t global_work_size = n;

	// create context to perform calculation with the specified device 
	context = clCreateContext(0, 1, &device, &pfn_notify, NULL, &err);
	//printf("clCreateContext %s\n", oclErrorString(err));
	assert(err == CL_SUCCESS);
	
	// create command queue for the context
	cmd_queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, NULL);
	
	char parameters[1024];
	sprintf(parameters, "#define NUMBER_OF_BODIES %d \n #define SOFTENING %ff \n#define GRAVCONST %ff \n", n, SOFTENING, G);
		
	// load program source from disk
	char *programSource = loadProgramSource(filename);
	char *fullSource = (char *)malloc(500*1024*sizeof(char));
	sprintf(fullSource, "%s %s", parameters, programSource);
	program[0] = clCreateProgramWithSource(context, 1, (const char**)&fullSource, NULL, &err);
	//printf("clCreateProgramWithSource %s\n", oclErrorString(err));
	assert(err == CL_SUCCESS);
		
	// build the program
	err = clBuildProgram(program[0], 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
		printf("clBuildProgram %s\n", oclErrorString(err));
		size_t len;
    	char buffer[2048];
    	printf("Error: Failed to build program executable!\n");
    	clGetProgramBuildInfo(program[0], device, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
   		printf("clGetProgramBuildInfo %s\n", buffer);
  		exit(1);
  	}
	
	// create kernels
	kernel[0] = clCreateKernel(program[0], "calculateForces", &err);
	kernel[1] = clCreateKernel(program[0], "updatePositions", &err);
	//printf("clCreateKernel %s\n", oclErrorString(err));
	assert(err == CL_SUCCESS);

	size_t buffer_size0 = sizeof(cl_float) * n;
	size_t buffer_size1 = sizeof(cl_int) * n;
	
	cl_mem x_mem, y_mem, ax_mem, ay_mem, vx_mem, vy_mem, mass_mem;
			
	// allocate memory
	float *ax = (float *)malloc(n*sizeof(float));
	float *ay = (float *)malloc(n*sizeof(float));

	// input arrays
	x_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, buffer_size0, NULL, NULL);
	y_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, buffer_size0, NULL, NULL);
	mass_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, buffer_size1, NULL, NULL);
	ax_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, buffer_size0, NULL, NULL);
	ay_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, buffer_size0, NULL, NULL);
	vx_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, buffer_size0, NULL, NULL);
	vy_mem = clCreateBuffer(context, CL_MEM_READ_WRITE, buffer_size0, NULL, NULL);
	
	printf("Global memory needed: %d\n", (int)(6*buffer_size0 + buffer_size1));
	
	int k;
	
	printf("Kernel work groups:\n");
	size_t kernel_work_groups[2] = {local_work_size};
	// kernel_work_group for 1st kernel must be local_work_size
	for (k=0; k<2; k++) {
		// get preferred work group sizes for kernels
		size_t kernel_work_group;
		clGetKernelWorkGroupInfo(kernel[k], device, CL_KERNEL_WORK_GROUP_SIZE, sizeof(kernel_work_group), &kernel_work_group, NULL);
		if (kernel_work_group<local_work_size) {
			kernel_work_group = local_work_size/2;
		}
		printf("%d: %d   ", k, (int)kernel_work_group);
		kernel_work_groups[k] = kernel_work_group;
	}
	printf("\n\n");
	 
	// remove old data.dat file
	remove(data);
	// print number of particles to new data.dat file
	fprintScalar(n);
	
	for (k=0; k<M; k++) {
		printf("%d\n",k);

		initialize(ax, ay);
		
		// write  position and mass to file data.dat
		fprintArrays(x,y,mass,n);
		
		err = clEnqueueWriteBuffer(cmd_queue, x_mem, CL_TRUE, 0, buffer_size0, (void*)x, 0, NULL, NULL);
		err |= clEnqueueWriteBuffer(cmd_queue, y_mem, CL_TRUE, 0, buffer_size0, (void*)y, 0, NULL, NULL);
		err |= clEnqueueWriteBuffer(cmd_queue, mass_mem, CL_TRUE, 0, buffer_size1, (void*)mass, 0, NULL, NULL);
		err |= clEnqueueWriteBuffer(cmd_queue, ax_mem, CL_TRUE, 0, buffer_size0, (void*)ax, 0, NULL, NULL);
		err |= clEnqueueWriteBuffer(cmd_queue, ay_mem, CL_TRUE, 0, buffer_size0, (void*)ay, 0, NULL, NULL);
		err |= clEnqueueWriteBuffer(cmd_queue, vx_mem, CL_TRUE, 0, buffer_size0, (void*)vx, 0, NULL, NULL);
		err |= clEnqueueWriteBuffer(cmd_queue, vy_mem, CL_TRUE, 0, buffer_size0, (void*)vy, 0, NULL, NULL);
		//printf("clEnqueueWriteBuffer %s\n", oclErrorString(err));
		assert(err == CL_SUCCESS);
		clFinish(cmd_queue);
				
		err = clSetKernelArg(kernel[0],  0, sizeof(cl_mem), &x_mem);
		err |= clSetKernelArg(kernel[0],  1, sizeof(cl_mem), &y_mem);
		err |= clSetKernelArg(kernel[0],  2, sizeof(cl_mem), &ax_mem);
		err |= clSetKernelArg(kernel[0],  3, sizeof(cl_mem), &ay_mem);
		err |= clSetKernelArg(kernel[0],  4, sizeof(cl_mem), &mass_mem);
		//printf("clSetKernelArg %s\n", oclErrorString(err));
		assert(err == CL_SUCCESS);
		
		err = clSetKernelArg(kernel[1],  0, sizeof(cl_mem), &x_mem);
		err |= clSetKernelArg(kernel[1],  1, sizeof(cl_mem), &y_mem);
		err |= clSetKernelArg(kernel[1],  2, sizeof(cl_mem), &vx_mem);
		err |= clSetKernelArg(kernel[1],  3, sizeof(cl_mem), &vy_mem);
		err |= clSetKernelArg(kernel[1],  4, sizeof(cl_mem), &ax_mem);
		err |= clSetKernelArg(kernel[1],  5, sizeof(cl_mem), &ay_mem);
		err |= clSetKernelArg(kernel[1],  6, sizeof(cl_mem), &mass_mem);
		//printf("clSetKernelArg %s\n", oclErrorString(err));
		assert(err == CL_SUCCESS);
		
		int j;
		// run the kernels
		for (j=0; j<2; j++) {
			long long start, end;
			cl_event event;
			
			size_t kernel_work_group = kernel_work_groups[j];
			err = clEnqueueNDRangeKernel(cmd_queue, kernel[j], 1, NULL, &global_work_size, &kernel_work_group, 0, NULL, &event);
			
			err |= clWaitForEvents(1, &event);
			//printf("clEnqueueNDRangeKernel %s\n", oclErrorString(err));
			assert(err == CL_SUCCESS);
			clFinish(cmd_queue);
			
			err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(start), &start, NULL);
			assert(err == CL_SUCCESS);
			
			err = clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(end), &end, NULL);
			assert(err == CL_SUCCESS);
			
			printf("kernel %d: %.6lf\n", j, (end-start)/1.0e9f);
			
			times[k][j] = (end-start)/1.0e9f;
			gputime += (end-start)/1.0e9f;
			
			clReleaseEvent(event);
		}
		
		printf("-------\n");
		
		// read back the results
		err = clEnqueueReadBuffer(cmd_queue, x_mem, CL_TRUE, 0, buffer_size0, x, 0, NULL, NULL);
		err |= clEnqueueReadBuffer(cmd_queue, y_mem, CL_TRUE, 0, buffer_size0, y, 0, NULL, NULL);
		err |= clEnqueueReadBuffer(cmd_queue, vx_mem, CL_TRUE, 0, buffer_size0, vx, 0, NULL, NULL);
		err |= clEnqueueReadBuffer(cmd_queue, vy_mem, CL_TRUE, 0, buffer_size0, vy, 0, NULL, NULL);
		err |= clEnqueueReadBuffer(cmd_queue, mass_mem, CL_TRUE, 0, buffer_size1, mass, 0, NULL, NULL);
		//printf("clEnqueueReadBuffer %s\n", oclErrorString(err));
		assert(err == CL_SUCCESS);
		clFinish(cmd_queue);
	}
	
	int j;
	for (j=0; j<2; j++) {
		float t = 0;
		for (k=0; k<M; k++) {
			t += times[k][j];
		}
		t /= M;
		printf("kernel %d average time: %f\n", j, t);
	}
	printf("total gpu time: %f\n", gputime);
	
	clReleaseMemObject(x_mem);
	clReleaseMemObject(y_mem);
	clReleaseMemObject(ax_mem);
	clReleaseMemObject(ay_mem);
	clReleaseMemObject(vx_mem);
	clReleaseMemObject(vy_mem);
	clReleaseMemObject(mass_mem);
	
	free(ax), free(ay);
	
	clReleaseProgram(program[0]);
	int i;
	for (i=0; i<2; i++) {
		clReleaseKernel(kernel[i]);
	}
	clReleaseCommandQueue(cmd_queue);
	clReleaseContext(context);
	return CL_SUCCESS;
}

int main (int argc, char *argv[]) {
	// declare time variables: 
	struct timeval start;
	struct timeval end;
    gettimeofday(&start, NULL);
	
	srand(3.0);
	
	// allocate memory for n particles
	float *x = (float *)malloc(n*sizeof(float));
	float *y = (float *)malloc(n*sizeof(float));
	float *vx = (float *)malloc(n*sizeof(float));
	float *vy = (float *)malloc(n*sizeof(float));
	int *mass = (int *)malloc(n*sizeof(int));
	
	float r, theta, v;
	
	// initialize positions, velocities and masses of bodies
	
	int i;
	for (i=0; i<n/4; i++) {
        r = plummerRand(0.5f,n/4.0f);
        theta = rand()/(float)RAND_MAX*8*atan(1.0f);

        x[i] = r*cos(theta)-0.4f;
        y[i] = r*sin(theta)-0.4f;

        v = orbitalSpeed(r, n/4.0f);
        vx[i] = v*cos(theta-atan(1.0f)*2.0f);
        vy[i] = v*sin(theta-atan(1.0f)*2.0f);

        vx[i] += 250.0f;
        vy[i] += 0.0f;
	}

	 for (i=n/4; i<n; i++) {
         r = plummerRand(1.2f,3*n/4.0f);
         theta = rand()/(float)RAND_MAX*8*atan(1.0f);

         x[i] = r*cos(theta)+1.4f;
         y[i] = r*sin(theta)+1.4f;

         v = orbitalSpeed(r, 3*n/4.0f);
         vx[i] = v*cos(theta-atan(1.0f)*2.0f);
         vy[i] = v*sin(theta-atan(1.0f)*2.0f);

         vx[i] += -200.0f;
         vy[i] += 0.0f;
    }

	for (i=0; i<n; i++) {
		mass[i] = 1;
	}
	
	// do the OpenCL calculation (M timesteps)
	runCL(x, y, vx, vy, mass);
	
	// free up memory
	free(x), free(y), free(vx), free(vy), free(mass);
	
	gettimeofday(&end, NULL);
	long t1 = start.tv_sec*1000000 + (start.tv_usec);
    long t2 = end.tv_sec*1000000  + (end.tv_usec);
    printf("total time: %f\n" ,(t2-t1)/1e6f);
    return 0;
}