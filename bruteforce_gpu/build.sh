echo Building...

UNAME=`uname`
if [ "$UNAME" == "Darwin" ]; then
	gcc -framework opencl -DAPPLE bruteforce_gpu.c -o bruteforce_gpu
else
	gcc -lOpenCL bruteforce_gpu.c -o bruteforce_gpu
fi