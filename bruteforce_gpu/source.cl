/*
 * Brute Force Simulation kernels
 * Teemu Peltonen, Ville Vaskonen
 */

#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable

#define BBLIMIT 0.0001f
#define DAMPING 0.00001f
#define TIMESTEP 0.0001f

__kernel void calculateForces(__global float *x, __global float *y, __global float *ax, __global float *ay, __global int *mass) {
	int gid = get_global_id(0);

	int pm = mass[gid];
	float px = x[gid];
	float py = y[gid];
	float dax = 0;
	float day = 0;
	float d;
	
	int j;
	for(j=0; j<NUMBER_OF_BODIES; j++) {
		d = (px-x[j])*(px-x[j]) + (py-y[j])*(py-y[j]);
		d = native_rsqrt(d+SOFTENING);
		d = GRAVCONST*mass[j]*d*d*d;
		dax += d*(x[j]-px);
		day += d*(y[j]-py);
	}
	
	ax[gid] += dax;
	ay[gid] += day;
}

__kernel void updatePositions(__global float *x, __global float *y, __global float *vx, __global float *vy,  
						__global float *ax, __global float *ay, __global int *mass) {
	int gid = get_global_id(0);
	
	int m = mass[gid];
	float pvx = vx[gid]; 
	float pvy = vy[gid];
	
	if(m!=0) {
		// velocity damping
		float v2 = pvx*pvx + pvy*pvy;
		float pax = ax[gid] -= sign(pvx)*DAMPING*v2/m;
		float pay = ay[gid] -= sign(pvy)*DAMPING*v2/m;
		
		// update position
		x[gid] += TIMESTEP*pvx + 0.5*pax*TIMESTEP*TIMESTEP;
		y[gid] += TIMESTEP*pvy + 0.5*pay*TIMESTEP*TIMESTEP;
		
		// calculate new velocity
		pvx += TIMESTEP*ax[gid];
		pvy += TIMESTEP*ay[gid];
		
		vx[gid] = pvx;
		vy[gid] = pvy;
	}
}
