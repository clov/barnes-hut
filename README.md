# Barnes-Hut


## Description
OpenCL implementation of the Barnes-Hut algorithm used in an N-body
simulation. Runs 100 % on GPU.

The actual Barnes-Hut OpenCL (running on GPU) implementation is under
`barneshut_gpu`. For performance comparison, a C implementation (running on
CPU) is under `barneshut_cpu` and a brute-force OpenCL (running on GPU)
implementation is under `bruteforce_gpu`.

For detailed explanation, see the
[report](https://www.dropbox.com/scl/fi/o55jqprd81uxxo946c921/TIEA342_peltonen_vaskonen_selostus.pdf)
(in Finnish).

Done as part of the course "TIEA342 - Programming of modern
multicore processors" at the University of Jyväskylä in 2011.

## Visuals
OpenGL-rendered [video](https://www.youtube.com/watch?v=XR20n3RK7PE) of a
simulation of 50k particles for 500 steps.

## Installation
TODO

## Usage
TODO

## Authors
Teemu Peltonen & Ville Vaskonen

## License
GNU General Public License v3.0
