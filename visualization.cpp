/*
 * Barnes-Hut N-body simulation visualization using SDL
 * (c) Teemu Peltonen & Ville Vaskonen
 */

#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include "SDL/SDL.h"

using namespace std;

#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1080

#define BRIGHTNESS 20
#define S 40 // scaling factor

#define SAVEBMP false // whether to save BMP files or not

void drawPixel(SDL_Surface *screen, int x, int y, Uint8 R, Uint8 G, Uint8 B) {
	Uint32 color = SDL_MapRGB(screen->format, R, G, B);

	Uint32 *bufp;
	bufp = (Uint32 *)screen->pixels + y*screen->pitch/4 + x;
	*bufp = color;
}

bool saveBMP(SDL_Surface *screen, const char *folder, const char *figName, const char *ext, int &stepIdx) {
	char figNameFull[1024];
	sprintf(figNameFull, "%s/%s%d%s", folder, figName, stepIdx, ext);
	if (SDL_SaveBMP(screen, figNameFull)<0) {
		return false;
	}
	stepIdx++;
	return true;
}

int main(int argc, char *argv[]){
	const char *data = "data.dat";
	const char *folder = "figs";
	const char *figName = "step";
	const char *ext = ".bmp";

    if (SAVEBMP) mkdir(folder, S_IRWXU | S_IRWXG | S_IRWXO);

	SDL_Surface *screen;
	SDL_Event event;

	const unsigned WIDTH = min(SCREEN_WIDTH, SCREEN_HEIGHT);
	const unsigned HEIGHT = WIDTH;
	
	// initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO)<0) {
		fprintf(stderr, "couldn't initialize SDL: %s\n",SDL_GetError());
		return(1);
	}
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_SWSURFACE|SDL_DOUBLEBUF);
	
	// open file
	ifstream file(data);
	if(!file) {
		cout << "failed to open data.dat" << endl;
		file.close();
		return(1);
	}
	
	int n;
	file >> n;
	
	float x, y;
	int m;
	
	Uint32 *pixels = (Uint32*)screen->pixels;
	Uint8 R, G, B;
	
    int stepIdx = 1000000;
	bool done = false;
	while (!done && !file.eof()) {
		// clear screen
		SDL_FillRect(screen, NULL, 0x000000);
		
		// draw next timestep
		for (int i=0; i<n; i++) {
			file >> x;
			file >> y;
			file >> m;
			
			x = (WIDTH/2 + x*S) + (SCREEN_WIDTH-WIDTH)/2;
			y = (HEIGHT/2 + y*S) + (SCREEN_HEIGHT-HEIGHT)/2;	
			
			if (m!=0 && x>0 && x<SCREEN_WIDTH && y>0 && y<SCREEN_HEIGHT) {
				int red, green, blue;
				SDL_GetRGB(pixels[(int)y*screen->pitch/4 + (int)x], screen->format, &R, &G, &B);
				red = R; green = G, blue = B;
				
				red += 3*BRIGHTNESS*m;
				if(red>250) R=250;
				else R = red;
				
				green += 2*BRIGHTNESS*m;
				if(green>240) G=240;
				else G = green;
				
				blue += 11*BRIGHTNESS*m;
				if(blue>220) B = 220;
				else B = blue;

				drawPixel(screen, x, y, R, G, B);
			}
		}

		SDL_Flip(screen);

		if (SAVEBMP) {
			if(!saveBMP(screen, folder, figName, ext, stepIdx)) {
        		cout << "failed to save BMP file" << endl;
				return(1);
			}
		}
		
		// check for events
		if (SDL_PollEvent(&event)) {
			if(event.type==SDL_QUIT || event.type==SDL_KEYDOWN)
				done = true;  
		}
	}
	
	file.close();
	SDL_Quit();
	return(0);
}