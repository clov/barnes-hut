/*
 * Barnes-Hut N-Body Simulation kernels
 * (c) Teemu Peltonen & Ville Vaskonen
 */

#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable

#define MAXDEPTH 32
#define THETA 0.25f
#define BBLIMIT 0.0001f
#define DAMPING 0.00001f
#define TIMESTEP 0.0001f

__kernel void boundingBox(__global float *x, __global float *y, __global float *gminx, 
		__global float *gminy, __global float *gmaxx, __global float *gmaxy, __global float *size) {
	int gid = get_global_id(0);
	int lid = get_local_id(0);

	float minx = x[0];
	float miny = y[0];
	float maxx = x[0];
	float maxy = y[0];
		
	__local float lminx[GROUP_SIZE], lminy[GROUP_SIZE], lmaxx[GROUP_SIZE], lmaxy[GROUP_SIZE];

	// number of threads used in first calculation
	int k = NUMBER_OF_BODIES/GROUP_SIZE;

	int j;
	if(gid<k) {
		for(j=gid; j<NUMBER_OF_BODIES; j+=k) {
			minx = min(minx,x[j]);
			miny = min(miny,y[j]);
			maxx = max(maxx,x[j]);
			maxy = max(maxy,y[j]);
		}
	}
		
	// write results to local memory
	lminx[lid] = minx;
	lminy[lid] = miny;
	lmaxx[lid] = maxx;
	lmaxy[lid] = maxy;

	write_mem_fence(CLK_LOCAL_MEM_FENCE);
	
	// calculate local minimum and maximum
	if(lid==0) {
		for(j=0; j<GROUP_SIZE; j++) {
			minx = min(minx,lminx[j]);
			miny = min(miny,lminy[j]);
			maxx = max(maxx,lmaxx[j]);
			maxy = max(maxy,lmaxy[j]);
		}
				
		// write results to global memory
		int group_id = get_group_id(0);
		gminx[group_id] = minx;
		gminy[group_id] = miny;
		gmaxx[group_id] = maxx;
		gmaxy[group_id] = maxy;
	}

	write_mem_fence(CLK_GLOBAL_MEM_FENCE);
	
	// calculate global minimum and maximum
	if(gid==0) {
		for(j=0; j<(float)k/GROUP_SIZE; j++) {
			minx = min(minx,gminx[j]);
			miny = min(miny,gminy[j]);
			maxx = max(maxx,gmaxx[j]);
			maxy = max(maxy,gmaxy[j]);
		}
		size[N-NUMBER_OF_BODIES-1] = max(maxx-minx, maxy-miny)/2.0f;
		x[N-1] = maxx - size[N-NUMBER_OF_BODIES-1];
		y[N-1] = maxy - size[N-NUMBER_OF_BODIES-1];
	}
}

int getSemaphor(__global int *semaphor) {
	return atom_xchg(semaphor, 1);
}

void releaseSemaphor(__global int *semaphor) {
	atom_xchg(semaphor, -1);
}

__kernel void buildTree(__global float *x, __global float *y, __global float *size, __global int *lu, 
				__global int *ru, __global int *ld, __global int *rd, __global int *semaphor, 
				__global int *cells_no, __global int *mass, __global float *vx, __global float *vy) {
	int gid = get_global_id(0);
		
	int j; // long index of the current node
	int k; // short index of the current node
	int prev, prevk; // previous j and k
	int a;
	
	float px = x[gid];
	float py = y[gid];
	
	int flag = 0;
	
	j = N-1;
	prev = N-1;
	k = j-NUMBER_OF_BODIES;
	
	// first thread inserts first body
	if (gid==0) {
		// find the quarter where the body belongs
		if(px<x[j]) {
			if(py<y[j])
				lu[k] = gid;
			else
				ld[k] = gid;
		} else {
			if(py<y[j])
				ru[k] = gid;
			else
				rd[k] = gid;
		}	
		flag = 1;
	}
	
	barrier(CLK_GLOBAL_MEM_FENCE);
	
	if(mass[gid]!=0) {
		while(flag!=1) {	
			// find the quarter where the body belongs
			while(j>=NUMBER_OF_BODIES) {
				if(semaphor[j]==-1) {
					prev = j;
					prevk = k;
					if(px<x[j]) {
						if(py<y[j])
							j = lu[k];
						else
							j = ld[k];
					} else {
						if(py<y[j])
							j = ru[k];
						else
							j = rd[k];
					}
					k = j-NUMBER_OF_BODIES;
				}
			}
			a = j = prev; 
			k = prevk;
			
			if(semaphor[j]==-1) {
				// try to lock
				if(getSemaphor(&semaphor[a])==-1) {	
					// find the quarter where the body belongs
					if(px<x[j]) {
						if(py<y[j])
							j = lu[k];
						else
							j = ld[k];
					} else {
						if(py<y[j])
							j = ru[k];
						else
							j = rd[k];
					}
					k = j-NUMBER_OF_BODIES;
					
					// if jth node is empty
					if(j<0) {
						// insert body
						if(px<x[prev]) {
							if(py<y[prev])
								lu[prevk] = gid;
							else
								ld[prevk] = gid;
						} else {
							if(py<y[prev])
								ru[prevk] = gid;
							else
								rd[prevk] = gid;
						}
						flag = 1;
					}
					
					// if j is a leaf node
					if(k<0 && j>=0) {
						prevk = prev-NUMBER_OF_BODIES;
						float psize  = size[prevk]/2.0f;
						// if bodies are very close each other
						if(psize<BBLIMIT) {
							// make one big body
							int m1 = mass[j];
							int m2 = mass[gid];
							vx[j] = (vx[j]*m1 + vx[gid]*m2)/(m1+m2);
							vy[j] = (vy[j]*m1 + vy[gid]*m2)/(m1+m2);
							vx[gid] = 0.0f;
							vy[gid] = 0.0f;
							mass[j] += m2;
							mass[gid] = 0;
							flag = 1;
						} else {
							int ncj, nck; 
							int cn = atom_inc(&cells_no[0]);
							ncj = N-cn-1; // new cell index in the long arrays
							nck = ncj-NUMBER_OF_BODIES; // new cell index in the short arrays
							
							// lock the new cell node
							getSemaphor(&semaphor[ncj]);
							
							// create a new internal node in the right place
							size[nck] = psize;
							if(lu[prevk]==j) {
								lu[prevk] = ncj;
								x[ncj] = x[prev]-psize;
								y[ncj] = y[prev]-psize;
							} else if(ru[prevk]==j) {
								ru[prevk] = ncj;
								x[ncj] = x[prev]+psize;
								y[ncj] = y[prev]-psize;
							} else if(ld[prevk]==j) {
								ld[prevk] = ncj;
								x[ncj] = x[prev]-psize;
								y[ncj] = y[prev]+psize;
							} else {
								rd[prevk] = ncj; 
								x[ncj] = x[prev]+psize;
								y[ncj] = y[prev]+psize;
							}
							// insert previous body
							if(x[j]<x[ncj]) {
								if(y[j]<y[ncj])
									lu[nck] = j;
								else
									ld[nck] = j;
							} else {
								if(y[j]<y[ncj])
									ru[nck] = j;
								else
									rd[nck] = j;
							}
							releaseSemaphor(&semaphor[ncj]);
							
							j = ncj;
							k = nck;
						}
					}
					write_mem_fence(CLK_GLOBAL_MEM_FENCE);
					
					releaseSemaphor(&semaphor[a]);
				}
			}
		}
	} 
}

__kernel void summarizeTree(__global float *x, __global float *y, __global int *lu, 
						__global int *ru, __global int *ld, __global int *rd, 
						__global int *mass, __global int *cells_no) {
	int gid = get_global_id(0);
	
	// index of the last body
	int k = N-cells_no[0]-NUMBER_OF_BODIES;
	int j = k + gid;
	
	int pmass = 0; 	// cumulative mass
	// center of mass
	float cx = 0; 
	float cy = 0;
	
	int counter = 0;
	int idx;
	
	// iterate over all internal nodes
	while(j<N-NUMBER_OF_BODIES) {
		counter = 0;
		// until all childs are ready
		while(counter<4) {
			cx = 0;
			cy = 0;
			counter = 0;
			pmass = 0;
			
			idx = lu[j];
			if(idx!=-1) {
				if(mass[idx]!=-1) {
					pmass += mass[idx];
					cx += mass[idx]*x[idx];
					cy += mass[idx]*y[idx];
					counter++;
				}
			} else counter++;
			
			idx = ru[j];
			if(idx!=-1) {
				if(mass[idx]!=-1) {
					pmass += mass[idx];
					cx += mass[idx]*x[idx];
					cy += mass[idx]*y[idx];
					counter++;
				}
			} else counter++;
			
			idx = ld[j];
			if(ld[j]!=-1) {
				if(mass[idx]!=-1) {
					pmass += mass[idx];
					cx += mass[idx]*x[idx];
					cy += mass[idx]*y[idx];
					counter++;
				}
			} else counter++;
			
			idx = rd[j];
			if(idx!=-1) {
				if(mass[idx]!=-1) {
					pmass += mass[idx];
					cx += mass[idx]*x[idx];
					cy += mass[idx]*y[idx];
					counter++;
				}
			} else counter++;
			
			if(counter==4) {
				// insert cumulative mass and center of mass to the internal node 
				x[j+NUMBER_OF_BODIES] = cx/pmass;
				y[j+NUMBER_OF_BODIES] = cy/pmass;
				write_mem_fence(CLK_GLOBAL_MEM_FENCE);
				mass[j+NUMBER_OF_BODIES] = pmass; 
			}
			
			write_mem_fence(CLK_GLOBAL_MEM_FENCE);
		}
		j += NUMBER_OF_BODIES;
	}
}

__kernel void sort(__global int *lu, __global int *ru, __global int *ld, 
				__global int *rd, __global int *mass, __global int *sorted) {
	int gid = get_global_id(0);
 
	int idx = N-1;
	int pmass = 0;
	// iterate over all internal nodes
	while(idx>NUMBER_OF_BODIES) {
		int cid = lu[idx-NUMBER_OF_BODIES];
		// check if node has childs
		if(cid!=-1) {
			pmass += mass[cid];
			// divide workitems according to the structure of the tree
			if(gid<pmass) {
				pmass -= mass[cid];
				idx = cid;
				continue;
			}
		}
		
		cid = ru[idx-NUMBER_OF_BODIES];
		if(cid!=-1) {
			pmass += mass[cid];
			if(gid<pmass) {
				pmass -= mass[cid];
				idx = cid;
				continue;
			}
		}
		
		cid = ld[idx-NUMBER_OF_BODIES];
		if(cid!=-1) {
			pmass += mass[cid];
			if(gid<pmass) {
				pmass -= mass[cid];
				idx = cid;
				continue;
			}
		}
		
		cid = rd[idx-NUMBER_OF_BODIES];
		if(cid!=-1) {
			pmass += mass[cid];
			if(gid<pmass) {
				pmass -= mass[cid];
				idx = cid;
				continue;
			}
		}
	}
	// there may be many workitems with the same pmass
	atom_xchg(&sorted[pmass],idx);
}

__kernel void calculateForces(__global float *x, __global float *y, __global float *ax, __global float *ay, 
						__global int *lu, __global int *ru, __global int *ld, __global int *rd, 
						__global int *mass, __global int *sorted, __global float *size) {
	int gid = get_global_id(0);
	int lid = get_local_id(0);
	// iteration stack
	__local int stack[4*MAXDEPTH*GROUP_SIZE/WARP_SIZE];
	__local float lx[GROUP_SIZE/WARP_SIZE], ly[GROUP_SIZE/WARP_SIZE]; 
	__local float lmass[GROUP_SIZE/WARP_SIZE], ls[GROUP_SIZE/WARP_SIZE];	
	__local int votes[GROUP_SIZE];

	int l;
	float d;
	
	// index of warp
	int j = (int)floor(lid/(float)WARP_SIZE);
	
	// initialize stack
	int k = j*4*MAXDEPTH;
	if(gid%WARP_SIZE==0)
		stack[k] = N-1;
	
	// get index of the body
	int idx = sorted[gid];
	float px = 0, py = 0, pax = 0, pay = 0;
	
	if(idx!=-1) {
		px = x[idx], py = y[idx];
	}
	while(k>=j*4*MAXDEPTH) {
		// check if node is empty
		if(stack[k]==-1) {
			k--;
			continue;
		}

		// first workitem in each warp reads data to local memory
		if(gid%WARP_SIZE==0) {
			lx[j] = x[stack[k]];
			ly[j] = y[stack[k]];
			lmass[j] = mass[stack[k]];
			if(stack[k]>=NUMBER_OF_BODIES)
			ls[j] = size[stack[k]-NUMBER_OF_BODIES];
		}
				
		// distance squared
		d = (px-lx[j])*(px-lx[j]) + (py-ly[j])*(py-ly[j]);
		
		// b=32 if all workitems in warp agree that node is far enough 
		int b = (idx==-1 || ls[j]*ls[j]<d*THETA);
		votes[lid] = b;
		int h;
		b = 0;
		for(h=j*WARP_SIZE; h<(j+1)*WARP_SIZE; h++) {
			b += votes[h];
		}
																																																						
		// check also if current node is a body
		if(stack[k]<NUMBER_OF_BODIES || b==32) {
			// check also that if node is a body it is not the body itself
			if(idx!=-1 && stack[k]!=idx) {
				d = native_rsqrt(d+SOFTENING);
				d = GRAVCONST*lmass[j]*d*d*d;
				pax += d*(lx[j]-px);
				pay += d*(ly[j]-py);
			}
			k--;
		} else {
			// first workitem in each warp inserts childs to iteration stack
			if(gid%WARP_SIZE==0) {
				l = stack[k]-NUMBER_OF_BODIES;
				stack[k++] = ld[l];
				stack[k++] = rd[l];
				stack[k++] = ru[l];
				stack[k] = lu[l];
			} else k += 3;
		}
	}
	if(idx!=-1) {
		ax[idx] = pax;
		ay[idx] = pay;
	}
}

__kernel void updatePositions(__global float *x, __global float *y, __global float *vx, __global float *vy,  
						__global float *ax, __global float *ay, __global int *mass) {
	int gid = get_global_id(0);
	
	int m = mass[gid];
	float pvx = vx[gid]; 
	float pvy = vy[gid];
	
	if(m!=0) {
		// velocity damping
		float v2 = pvx*pvx + pvy*pvy;
		float pax = ax[gid] -= sign(pvx)*DAMPING*v2/m;
		float pay = ay[gid] -= sign(pvy)*DAMPING*v2/m;
		
		// update position
		x[gid] += TIMESTEP*pvx + 0.5*pax*TIMESTEP*TIMESTEP;
		y[gid] += TIMESTEP*pvy + 0.5*pay*TIMESTEP*TIMESTEP;
		
		// calculate new velocity
		pvx += TIMESTEP*ax[gid];
		pvy += TIMESTEP*ay[gid];
		
		vx[gid] = pvx;
		vy[gid] = pvy;
	}
}
