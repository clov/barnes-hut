echo Building...

UNAME=`uname`
if [ "$UNAME" == "Darwin" ]; then
	gcc -framework opencl -DAPPLE barneshut_gpu.c -o barneshut_gpu
else
	gcc -lOpenCL barneshut_gpu.c -o barneshut_gpu
fi